# Instructions
## In the clone repo
### Build the Spring Boot container
`docker build -t springdemo:nodf -f ./Dockerfile .`
### Replace the DF_RUN_TOKEN
Replace the DF_RUN_TOKEN in the Dockerfile.alpine.df file with your own run token value.
https://gitlab.com/jkday/springdemo/-/blob/master/Dockerfile.alpine.df#L17
### Instrument the container
`docker build -t springdemo:df -f Dockerfile.alpine.df --build-arg "APP_IMAGE=springdemo:nodf" --build-arg "DF_APP_NAME=demos" --build-arg "DF_COMPONENT=springdemo" --no-cache .`
### Run the container
`docker run -ti --rm -p 8888:80 springdemo:df`
### Verify Spring Boot web app
`curl localhost:8888`